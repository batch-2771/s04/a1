


-- Find all artists that has letter d in its name.
SELECT * FROM artists WHERE name LIKE "%d%";

-- Find all songs that has a length of less than 230.
SELECT * FROM songs WHERE length < 230;

-- Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)

SELECT albums.album_title, songs.song_name, songs.length FROM albums JOIN songs ON albums.id = songs.album_id;


-- Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name.)

SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id WHERE albums.album_title LIKE "%a%";

-- Sort the albums in Z-A order. (Show only the first 4 records.)

SELECT *(4) FROM albums ORDER BY album_title DESC;

-- Join the 'albums' and 'songs' tables. (Sort albums from Z-A)

SELECT * FROM albums JOIN songs ON albums.id = songs.album_id ORDER BY album_title DESC;